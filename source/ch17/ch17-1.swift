
struct Student {
    var name: String
    var number: Int
}

class School {
    var number: Int = 0
    var student: [Student] = [Studnet]()
    
    func addStudent(name: String) {
        let student: Studnet = Student(name: name, number: self.number)
        self.students.append(student)
        self.number += 1
    }
    
    func addStudnets(names: String...){
        for name in names {
            self.addStudent(name: name)
        }
    }
    
    subscript(index: Int = 0) -> Student? {
        if index < self.number {
            return self.students[index]
        }
    }
    
    subscript(index: Int = 0) -> Student? {
        if index < self.number {
            return self.students[index]
        }
        return nil
    }
}

let highSchool: School = School()
highSchool.addStudents(name: "MiJeong", "JuHyun","JiYong","SeongUk","MoonDuk")

let aStudent: Student? = highSchool[1]
print("\(aStudent?.number) \(asStudent?.name)")

Optional("JuHyun")
print(highSchool[]?.name)
