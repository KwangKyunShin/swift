//
//  ch4-5-6.swift
//  MyPgm
//
//  Created by KWANG KYUN SHIN on 2021. 7. 25..
//  Copyright © 2021년 KWANG KYUN SHIN. All rights reserved.
//

import Foundation
import Swift

enum Condition: Comparable {
    case terrible
    case bad
    case good
    case great
}

let myCondition: Condition = Condition.great
let yourCondition: Condition = Condition.bad

if myCondition >= yourCondition {
    print("My Condition is more good! ")
}else{
    print("당신의 상태가 더 좋아요")
}


enum Device: Comparable {
    case iPhone(version: String)
    case iPad(version: String)
    case macBook
    case iMac
}

var devices: [DEVICE] = []
device.append(Device.iMac)
device.append(Device.iPhone(version: "14.3"))
device.append(Device.iPhone(version: "6.1"))
device.append(Device.iPad(version: "10.3"))
device.append(Device.macBook)


let sortedDevices: [Device] = devices.sorted()
print(sortedDevices)


