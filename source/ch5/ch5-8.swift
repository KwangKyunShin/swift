prefix func ! (value: String) -> Bool {
    return value.isEmpty
}


var stringValue: String = "yagom"
var isEmptyString: Bool = !stringValue

print(isEmptyString)

stringValue = ""
isEmptyString = !stringValue

print(isEmptyString)


prefix operator **

prefix func ** (value: String) -> String {

    return value + " " + value
}

let resultString: String = **"yagom"

print(resultString)

postfix operator **

postfix func ** (value: Int) -> Int{

    return value + 10
}

let five: Int = 5
let fivePlusTen: Int = five**

print(fivePlusTen)


prefix operator ***
postfix operator ***

prefix func *** (value: Int) -> Int{
    return value * value
}

postfix func *** (value: Int) -> Int{
    return value + 1
}

let six: Int = 6
let sqrtSixPlusTem: Int = ***six***

print(sqrtSixPlusTem)



rtSixPlusTem)



