protocol SelfPrintable {
	func printSelf()
}


extension SelfPrintable where Self: Container {
	func printSelf() {
		print(items)
	}
}

protocol Container: SelfPrintable {
	associatedtype ItemType

	var items: [ItemType] { get set}
	var count: Int { get }

	mutating func append(item: ItemType)
	subscript(i: Int) -> ItemType {
		get 
	}
}

extension Container {
	mutating func append(item: ItemType) {
		items.append(item)
	}

	var count: Int {
		return items.count
	}

	subscript(i: Int) -> ItemType {
		return items[i]
	}
}

protocol Popable: Container {
	mutating func pop() -> ItemType?
	mutating func push(_ item: ItemType)
}

extension Popable {
	mutating func pop() -> ItemType?{
		return items.removeLast()
	}
	mutating func push(_ item: ItemType) {
		self.append(item: item)
	}
}

protocol Insertable: Container{
	mutating func delete() -> ItemType?
	mutating func insert(_ item: ItemType)
}


extension Instable {
	mutating func delete() -> ItemType? {
		return items.removeFirst
	}
	mutating func insert(_ item: ItemType) {
		self.append(item: item)
	}
}

struct Stack<Element>: Popable {
	var items: [Element] = [Element]()
}

struct Queue<Element>: Insertable {
	var items: [Element] = [Element]()
}

var myIntStack: Stack<Int> = Stack<Int>()






















