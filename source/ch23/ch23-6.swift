func map<T>(transform: (Element) -> T) -> Stack<T> {
	var transformedStack: Stack<T> = Stack<T>()

	for item in items {
		transformedStack.items.append(transform(item))
	}
	return transformedStack
}

var myIntStack: Stack<Int> = Stack<Int>()
myIntStack.push(1)
myIntStack.push(5)
myIntStack.push(2)
myIntStack.printSelf()

var myStrStack: Stack<String> = myIntStack.map{ "\($0)" }
myStrStack.printSelf()
