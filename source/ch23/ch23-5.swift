let items: Array<Int> = [1, 2, 3]
let mappedItems: Array<Int> = items.map { (item: Int) -> Int in
    return item * 10    
}

print(mappedItems)
