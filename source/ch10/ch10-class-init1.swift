
struct CoordinatePoint {
    var x: Int = 0
    var y: Int = 0
}


let yagomPoint: CoordinatePoint = CoordinatePoint()
let wizplanPoint: CoordinatePoint = CoordinatePoint(x: 10, y: 5)

print("yagom's point : \(yagomPoint.x), \(yagomPoint.y)")

print("yagom's point : \(wizplanPoint.x), \(wizplanPoint.y)")

class Position {
    var point: CoordinatePoint = CoordinatePoint()
    var name: String = "Unknown"
/*
    init(name: String, currentPoint: CoordinatePoint) {
        self.name = name
        self.point = currentPoint
    }
 */
}

let yagomPosition: Position = Position()

yagomPosition.point = yagomPoint
yagomPosition.name = "yagom"

//let yagomPosition: Position = Position(name: "yagom", currentPoint: yagomPoint)

struct CoordinatePoint1{
    var x: Int
    var y: Int
}

class Position1 {
    var point: CoordinatePoint1?
    let name: String

    init(name: String) {
        self.name = name
    }
}

let yagomPosition1: Position1 = Position1(name: "yagom")

yagomPosition1.point = CoordinatePoint1(x: 20, y: 10)
1(x: 20, y: 10)
