
class Account {
    var credit: Int = 0 {
        willSet {
            print("잔액이 \(credit) 에서 \(newValue) 으로 변경될 예정 입니다.")
        }
        didSet {
            print("잔액이 \(oldValue) 에서 \(credit) 으로 변경되었습니다.")
        }
    }

    var dollarValue: Double {
        get {
            Double(credit) / 1000.0
        }

        set {
            credit = Int(newValue * 1000)
            print("잔액을 \(newValue) 달라로 변경 중입니다")
        }
    }

}

class ForeignAccount: Account {
    override var dollarValue: Double {
        willSet {
            print("잔액이 \(dollarValue) 에서 \(newValue) 으로 변경예정입니다.")
        }
        didSet {
            print("잔액이 \(oldValue) 에서 \(dollarValue) 으로 변경되었습니다.")
        }
    }
}


let myAccount: ForeignAccount = ForeignAccount()

myAccount.credit = 1000

myAccount.dollarValue = 2


