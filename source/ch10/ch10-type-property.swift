class AClass {
    static var typeProperty: Int = 0

    var instanceProperty: Int = 0 {
        didSet {
            Self.typeProperty = instanceProperty + 100
        }
    }

    static var typeComputerdProperty: Int {
        get {
            return typeProperty
        }
        set {
            typeProperty = newValue
        }
    }
}

AClass.typeProperty = 123

let classInstance: AClass = AClass()

classInstance.instanceProperty = 100

print("test \(AClass.typeProperty)")
print(AClass.typeComputerdProperty)
puterdProperty)
