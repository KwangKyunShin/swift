struct Person {
    let name: String
    let nickname: String?
    let age: Int

    var isAdult: Bool {
        return age > 10
    }
}


let yagom: Person = Person(name: "yagom", nickname:"bear", age: 100)
let hana: Person = Person(name: "hana", nickname: "na", age: 100)
let happy: Person = Person(name: "happy", nickname: nil, age: 3)

let family: [Person] = [yagom, hana, happy]
let names: [String] = family.map(\.name)
let nickname: [String] = family.compactMap(\.nickname)
let adults: [String] = family.filter(\.isAdult).map(\.name)
ult).map(\.name)
