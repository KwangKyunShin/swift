class Person {
    var name: String
    init(name: String) {
        self.name = name
    }
}


struct Stuff {
    var name: String
    var owner: Person
}

print(type(of: \Person.name))
print(type(of: \Stuff.name))

var keyPath = \Stuff.owner
var  nameKeyPath = keyPath.appending(path: \.name)


print(keyPath)
print(nameKeyPath)


let yagom = Person(name: "yagom")
let hana = Person(name: "hana")
let macbook = Stuff(name: "MacBook Pro", owner: yagom)
var iMac = Stuff(name: "iMac", owner: yagom)
let iPhone = Stuff(name: "iPhone", owner:hana)

let stuffNameKeyPath = \Stuff.name
let ownerkeyPath = \Stuff.owner

let ownerNameKeyPath = ownerkeyPath.appending(path: \.name)

print(macbook[keyPath: stuffNameKeyPath])
    print(iMac[keyPath: stuffNameKeyPath])
    print(iPhone[keyPath: stuffNameKeyPath])

    print(macbook[keyPath: ownerNameKeyPath])
    print(iMac[keyPath: ownerNameKeyPath])
    print(iPhone[keyPath: ownerNameKeyPath])

    iMac[keyPath: stuffNameKeyPath] = "iMacPro"
    iMac[keyPath: ownerkeyPath] = hana

    print(iMac[keyPath: stuffNameKeyPath])
    print(iMac[keyPath: ownerNameKeyPath])







︀