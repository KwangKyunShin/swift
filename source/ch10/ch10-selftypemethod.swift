struct SystemVolume {
    static var volume: Int = 5

    static func mute(){
        self.volume = 0
    }
}


class Navigation {
    var volume: Int = 5

    func guideWay() {
        SystemVolume.mute()
    }

    func funishGuideWay() {
        SystemVolume.volume = self.volume
    }
}

SystemVolume.volume = 10

let myNavi: Navigation = Navigation()

myNavi.guideWay()
print(SystemVolume.volume)

myNavi.funishGuideWay()
print(SystemVolume.volume)
