
class Room{
    var number: Int
    init(number: Int){
        self.number = number
    }
}

class Building {
    var name: String
    var room: Room?
    init(name: String){
        self.name = name
    }
}
struct Address {
    var province: String
    var city: String
    var street: String
    var bulding: Building?
    var detailAddress: String?
    
    init(province: String, city: String, street: String) {
        self.province = province
        self.city = city
        self.street = street
    }
}

class Person {
    var name: String
    var address: Address?
    init(name: String) {
        self.name = name
    }
}

let yagom: Person = Person(name: "yagom")

