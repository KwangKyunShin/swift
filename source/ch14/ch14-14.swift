


func enterClub(name: String?, age: Int?) {
    guard let name: String = name, let age: Int = age, age > 19,name.isEmpty == false else {
        print("You are too young to enter the club")
        return
    }
    print("Welcome \(name)")
}

let first: Int = 3
let second: Int = 5

func myfunc(){
    guard first > second else {
        print("false")
        return
    }
}

myfunc()

