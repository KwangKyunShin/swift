class Person {
    var height: Float = 0.0
    var weight: Float = 0.0
}

var yagom: Person = Person()
let friend: Person = yagom
let anotherFriend: Person = Person()

print(yagom === friend)
print(yagom === anotherFriend)
print(friend !== anotherFriend)

湵汤e