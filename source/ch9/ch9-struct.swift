

struct BasicInformation {
    var name: String
    var age: Int
}


var yagomInfo: BasicInformation = BasicInformation(name: "yagom", age: 99)

yagomInfo.age = 90
yagomInfo.name = "Seba"

let sebaInfo: BasicInformation = BasicInformation(name: "Seba", age: 99)
//sebaInfo.age = 100 //변경 불가 오류 !
//jennyInfo.age = 100


//var jennyInfo: BasicInformation = BasicInformation()
//jennyInfo.name = "Seba"
//jennyInfo.age = 90


//////////////////////////////////////////////////////////////////////////


class Person {
    var height: Float = 0.0
    var weight: Float = 0.0
}


var yagom: Person = Person()
yagom.height = 123.4
yagom.weight = 123.4

let jenny: Person = Person()
jenny.height = 123.4
jenny.weight = 123.4

//////////////////////////////////////////////////////////////////////////

class PersonN {
    var height: Float = 0.0
    var weight: Float = 0.0

    deinit {
        print("PersonN 클래스의 인스턴스가 소멸 됩니다.")
    }
}

var yagomN: PersonN? = PersonN()
yagomN = nil














