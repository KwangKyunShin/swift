
let integerValue: Int = 5

switch integerValue {
case 0:
    print("value == Zero")
case 1...10:
    print("value == 1 ~ 10")
    fallthrough
case Int.min ..< 0 , 101 ..< Int.max:
    print("value < 0 or value > 100")
    break
default:
    print("10 < Value <= 100")
}


let doubleValue: Double = 3.0

switch doubleValue{
case 0:
    print("value == Zero")
case 1.5 ... 10.5:
    print("1.5 <= Value <= 10.5")
default:
    print("value == \(doubleValue)")
}

let stringValue: String = "Liam Neeson"

switch stringValue{
case "yagom":
    print("He is yagom")
case "Jay":
    print("He is Jay")
case "Jenny","Joker","Nova":
    print("He or she is \(stringValue)")
default:
    print("\(stringValue) said 'I don't know who you are'")
}

typealias NameAge = (name: String, age: Int)

let tupleValue: NameAge = ("yagom2",100)

switch tupleValue{
    case ("yagom",50): print("정확히 맞췄습니다")
    case ("yagom1", _): print("이름만 맞았습니다. 나이는 \(tupleValue.age) 입니다.")
    case ("yagom", let age): print("이름만 맞았습니다. 나이는 \(age) 입니다.")
    case (_,99): print("나이만 맞았습니다1. 이름은 \(tupleValue.name) 입니다.")
    case (let name,100): print("나이만 맞았습니다2. 이름은 \(name) 입니다.")
    default:print("누굴 찾나요?")
}


