
let 직급: String = "사원"
let 연차: Int = 1
let 인턴인가: Bool = false

switch 직급 {
case "사원" where 인턴인가 == true: print("인턴입니다.")
case "사원" where 연차 < 2 && 인턴인가 == false: print("신입사원입니다.")
case "사원" where 연차 > 5 : print("연식 쫌 된 사원입니다")
case "사원": print("사원입니다.")
case "대리": print("대리입니다.")
default: print("사장입니까?")
}


enum School {
    case primary
    case elementary
    case middle
    case high
    case collage
    case university
    case graduate
}

let 최종학력 : School = School.university

switch 최종학력 {
case .primary: print("최종학력은 유치원입니다.")
case .elementary : print("최종학력은 초등")
case .middle: print("중딩")
case .high: print("고딩")
case .collage, .university: print("대학(교)")
case .graduate : print("대학원")
}


enum Menu{
    case chicken
    case pizza
    case hambuger
}

let lunchMenu: Menu = .chicken

switch lunchMenu {
case .chicken: print("반반 무많이")
case .pizza: print("핫소스 많이")
@unknown case _:print("오늘은 메뉴가 뭐죠?") //default: 와 같은 표현
}














