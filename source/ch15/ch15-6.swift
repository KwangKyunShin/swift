
let numbers: [Int] = [1, 2, 3]


var sum: Int = numbers.reduce(0, {(result: Int, next: Int) -> Int in
    print("\(result) + \(next)")
    return result + next
})

print(sum)

let subtract: Int = numbers.reduce(0, {(result: Int, next: Int) -> Int in
    print("\(result) - \(next)")
    return result - next
})

print(subtract)

let sumFromThree: Int = numbers.reduce(3) {
    print("\($0) + \($1)")
    return $0 + $1
}

print(sumFromThree)

var subtractFromThree: Int = numbers.reduce(3) {
    print("\($0) - \($1)")
    return $0 - $1
}

print(subtractFromThree)

let names: [String] = ["Chope", "Jay", "Joker", "Nova"]
let reducedNames: String = names.reduce("yagom's friend : ") {
    return $0 + ", " + $1
}

print(reducedNames)

sum = numbers.reduce(into: 0 , { ( result: inout Int, next: Int) in
    print("\(result) + \(next)")
    result += next
})


print(sum)

subtractFromThree = numbers.reduce(into: 3, {
    print("\($0) - \($1)")
    $0 -= $1
})

var doubleNumbers: [Int] = numbers.reduce(into: [1, 2]) { (result: inout [Int], next: Int) in
    print("result: \(result) next: \(next)")
    
    guard next != nil else {
        return
    }
    
    print("\(result) append \(next)")
    result.append(next * 2)
}

print(doubleNumbers)

doubleNumbers = [1, 2] + numbers.filter { $0.isMultiple(of: 2)}.map{ $0 * 2}
print(doubleNumbers)

var upperCasedNames: [String]
upperCasedNames = names.reduce(into: [], {
    $0.append($1.uppercased())
})

print(upperCasedNames)

upperCasedNames = names.map { $0.uppercased() }
print(upperCasedNames)

