
let numbers: [Int] = [1,2,3,4,5,6,7]

var result: Int = numbers.filter{ $0.isMultiple(of: 2)}.map{$0 * 3}.reduce(0) { $0 + $1}
print(result)

result = 0

for number in numbers {
    guard number.isMultiple(of: 2) else {
        continue
    }
    result += number * 3
}

print(result)
