class SomeClass {
    deinit {
        print("Instance will be deallocated Immediagely")
    }
}

var instance: SomeClass? = SomeClass()
instance = nil
