 
struct Area {
    var squareMeter: Double

    init() {
        self.squareMeter = 0.0
    }

    init(fromPy py: Double) {
        squareMeter = py * 3.3058
    }

    init(fromSquareMeter squareMeter: Double) {
        self.squareMeter = squareMeter
    }

    init(value : Double){
        squareMeter = value
    }

    init(_ value: Double){
        squareMeter = value
    }

}

let room: Area = Area()
print(room.squareMeter)

let roomOne: Area = Area(fromPy: 15.0)
print(roomOne.squareMeter)

let roomTwo: Area = Area(fromSquareMeter: 33.06)
print(roomTwo.squareMeter)

let roomThree: Area = Area(value: 30.0)

let roomFour: Area = Area(55.0)



= Area(55.0)



