//ch28-4.swift
for i in 0...2 {
    defer {
        print("A", terminator:" ")
    }
    print(i, terminator:  " ")
    
    if i % 2 == 0 {
        defer {
            print("", terminator: "\n")
        }
        print("It's even", terminator: " ")
    }
}


func someThrowingFunction(shouldThrowError: Bool) throws -> Int {
    defer {
        print("First")
    }
    
    if shouldThrowError {
        enum SomeError: Error{
            case justSomeError
        }
        throw SomeError.justSomeError
    }
    
    defer {
        print("second")
    }
    
    defer {
        print("Third")
    }
    
    return 100
}

try? someThrowingFunction(shouldThrowError: true)

try? someThrowingFunction(shouldThrowError: false)

//////////////////////////////////////////////////////////////////

func someFunction() {
	print("1")

	defer {
		print("2")
	}
	do {
		defer {
			print("3")
		}
		print("4")
	}
	defer {
		print("5")
	}

	print("6")
}

someFunction()



