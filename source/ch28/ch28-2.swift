//ch28-2.swift

func buyFavoriteSnack(person: String, vendingMachine: VendingMachine) {
	let snackname = favoriteSnacks[person] ?? "Candy Bar"
	tryingVend(itemNamed: snackName, vendingmachine: vendingMachine)
}

struct PurchasedSnack {
	let name: String
	init(name: String, vendingmachine: VendingMachine) {
		tryingVend(itemNamed: name, vendingMachine: vendingMachine)
		self.name = name
	}
}

func tryingVend(itemNamed: String, vendingMachine: VendingMachine) {
	do {
		try vendingMachine.vend(itemNamed: itemNamed)
	}catch VendingMachineError.invalidSelection {
		print("유효하지 않는 선택")
	}catch VendingMachineError.outOfStock {
		print(" 품절 ")
	}catch VendingMachineError.insufficientFunds(let coinsNeeded) {
		print("자금 부족 - 동전 \(coinsNeeded) 개를 추가로 지급해주세요")
	}catch {
		print("그 외 오류 발생:", error)
	}
}

let machine: VendingMachine = VendingMachine() 
machine.coinsDeposited = 20

var purchase: PurchasedSnack = PurchasedSnack(name: "Biscuit", vendingMachine: machine)

print(purchase.name)

purchase = PurchasedSnack(name: "Ice Cream", vendingMachine: machine)

print(purchase.name)

for(person, favoriteSnack) in favoriteSnacks {
	print(person, favoriteSnack)
	try buyFavoriteSnack(person: person, vendingMachine: machine)
}