prefix operator ** 

prefix func ** <T: BinaryInteger> (value: T) -> T {
    return value * value
}

let minusFive: Int = -5
let sqrtMinusFive: Int = **minusFive

print(sqrtMinusFive)

let five: UInt = 5

let sqrtFive: UInt = **five
print(sqrtFive)
