
func swapTwoInts(_ a: inout Int, _ b: inout Int) {
    let temporaryA: Int = a
    a = b
    b = temporaryA
}
var numberOne: Int = 5
var numberTwo: Int = 10

swapTwoInts(&numberOne, &numberTwo)
print("\(numberOne), \(numberTwo)")


func swapTwo<T: Any>(_ a: inout T, _ b: inout T) {
    let temporaryA: T = a
    a = b
    b = temporaryA
}

var one: Any = "5"
var two: Any = "10"
print("\(one), \(two)")
swapTwo(&one, &two)
print("\(one), \(two)")

swap(&one, &two)
print("\(one), \(two)")


///////////////////////////////////////////

//func swapTwoValues<T: BinaryInteger> (_ a: inout T, _ b: inout T ) where T: FloatingPoint{
func swapTwoValues<T> (_ a: inout T, _ b: inout T ) {
    let temporaryA: T = a
    a = b
    b = temporaryA
}

swapTwoValues(&numberOne,&numberTwo)
print("swapTwoValues:\(numberOne), \(numberTwo)")

swapTwoValues(&one,&two)
print("swapTwoValues:\(one), \(two)")



func makeDictionaryWithTwoValue<Key: Hashable, Value>(key: Key, value: Value) -> Dictionary<Key, Value> {
    let dictionary: Dictionary<Key, Value> = [key: value]
    return dictionary
}

let d: Dictionary = makeDictionaryWithTwoValue(key: "key",value: 10)

print(d)




