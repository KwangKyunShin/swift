protocol Container {
    associatedtype ItemType
    var count: Int { get }
    mutating func append(_ item: ItemType)
    subscript(i: Int) -> ItemType { get }
}

class MyContainer: Container {
    var items: Array<Int> = Array<Int>()
    var count: Int{
        return items.count
    }
    func append(_ item: Int) {
        items.append(item)
    }
    subscript(i: Int) -> Int {
        return items[i]
    }
}

struct IntStack: Container {
    
    typealias ItemType = Int
    
    var items = [ItemType]()
    mutating func push(_ item: ItemType){
        items.append(item)
    }
    mutating func pop() -> ItemType {
        return items.removeLast()
    }
    
    mutating func append(_ item: ItemType) {
        self.push(item)
    }
    
    var count: ItemType {
        return items.count
    }
    
    subscript(i: ItemType) -> ItemType {
        return items[i]
    }
}


struct Stack<Element>: Container {
    var items = [Element]()
    mutating func push(_ item: Element) {
        items.append(item)
    }
    
    mutating func pop() -> Element {
        return items.removeLast()
    }
    
    mutating func append(_ item: Element) {
        self.push(item)
    }
    
    var count: Int{
        return items.count
    }
    subscript(i: Int) -> Element {
        return items[i]
    }
}

extension Stack {
    subscript<Indices: Sequence> (indices: Indices) -> [Element] where Indices.Iterator.Element == Int {
        var result = [ItemType]()
        for index in indices {
            result.append(self[index])
        }
        return result
    }
}

var integerStack: Stack<Int> = Stack<Int>()
integerStack.append(1)
integerStack.append(2)
integerStack.append(3)
integerStack.append(4)
integerStack.append(5)
integerStack.append(6)

print(integerStack[0...2])
