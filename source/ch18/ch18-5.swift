class Person {
    var name: String
    var age: Int

    init(name: String, age: Int){
        self.name = name
        self.age = age
    }
}

class Student: Person {
    var major: String
    init(name: String, age: Int, major: String){
        self.major = "Swift"
        super.init(name: name, age: age)
    }

    convenience init(name: String) {
        self.init(name: name, age: 7, major: "")
    }

    func str() -> String{
        let str = "name:\(super.name) age:\(super.age) major:\(self.major)"
        print(str)
        return str
    }
}

var p: Student = Student(name: "Std")
print("AA:\(p.str()):AA")

\(p.str()):AA")

