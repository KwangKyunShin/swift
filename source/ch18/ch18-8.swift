class Person {
    var name: String

    required init(){
        self.name = "name"
    }

//    convenience init(){
//        self.init(name: "Unknown")
//    }
}

class Student: Person{
    var major: String =  "Swift"
    init(major: String) {
        self.major = major
        super.init()
    }
    required init() {
        self.major = "Unknown"
        super.init()
    }
//    init(name: String, major: String) {
//        self.major = major
//        super.init(name: name)
//    }
}

class UniversityStudent: Student {
    var grade: String

    init(grade: String) {
        self.grade = grade
        super.init()
    }
    required init() {
        self.grade = "F"
        super.init()
    }
}

let jiSoo: Student = Student()
print(jiSoo.major)

let yagom: Student = Student(major: "Swift")
print(yagom.major)

let juHyun: UniversityStudent = UniversityStudent(grade: "A+")
print(juHyun.grade)



//let yagom: Person = Person(name: "yagom")
//let hana: Student = Student(name: "hana")

//print(yagom.name)
//print(hana.name)

let wizplan: Person = Person()
let jinSung: Student = Student()

print(wizplan.name)
print(jinSung.name)
t(jinSung.name)
