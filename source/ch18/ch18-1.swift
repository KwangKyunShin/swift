


class Person {
    var name: String = ""
    var age : Int = 0
    var koreanAge: Int {
        return self.age + 1
    }
    var introduction: String {
        return "name: \(name), age: \(age) koreanAge: \(koreanAge)"
    }
    
    func speak() {
        print("abcdefg")
    }
    class func introduceClass() -> String {
        return "we wish peace"
    }
}

let yagom: Person = Person()
yagom.name = "yagom"
yagom.age = 55
//yagom.koreanAge = 56
print(yagom.introduction)
yagom.speak()

class Student : Person {
    var grade: String = "F"
    override var introduction: String {
        return super.introduction + "  " + "score : \(self.grade)"
    }
    override var koreanAge: Int {
        get {
            return super.koreanAge
        }
        set {
            self.age = newValue - 1
        }
    }
    func study() {
        print("Study hard...")
    }
    override func speak(){
        print("I'm student")
    }
}

let jay: Student = Student()
jay.name = "jay"
jay.age = 42
jay.grade = "A"
jay.koreanAge = 20
print(jay.introduction)
jay.speak()
jay.study()

class UniversityStudent: Student {
    var major: String = ""
    class func introduceClass() {
        print(super.introduceClass())
    }
    override class func introduceClass() -> String {
        return "I want A+"
    }
}

let jenny: UniversityStudent = UniversityStudent()
jenny.major = "Art"
jenny.speak()
jenny.study()

print(Person.introduceClass())
print(Student.introduceClass())
print(UniversityStudent.introduceClass() as String)
UniversityStudent.introduceClass() as Void
