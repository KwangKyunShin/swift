class Person {
    var name: String

    init(name: String){
        self.name = name
    }

    convenience init(){
        self.init(name: "Unknown")
    }
}

class Student: Person{
    var major: String =  "Swift"
}

let yagom: Person = Person(name: "yagom")
let hana: Student = Student(name: "hana")

print(yagom.name)
print(hana.name)

let wizplan: Person = Person()
let jinSung: Student = Student()

print(wizplan.name)
print(jinSung.name)
