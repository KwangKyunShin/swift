class Person {
    var name: String
    var age: Int

    init() {
        self.name = "Unknown"
        self.age = 0
    }
    init?(name: String, age: Int){
        if name.isEmpty {
            return nil
        }
        self.name = name
        self.age = age
    }

    init?(age: Int){
        if age < 0 {
            return nil
        }
        self.name = "Unknown"
        self.age = age
    }
//
//    convenience init(name: String) {
//        self.init(name: name, age: 0)
//    }
}

class Student: Person {
    var major: String
    override init?(name: String, age: Int){
        self.major = "Swift"
        super.init(name: name, age: age)
    }
    override init(age: Int){
        self.major = "Swift"
        super.init()
    }

    func str() -> String{
        let str = "name:\(super.name) age:\(super.age) major:\(self.major)"
        print(str)
        return str
    }
}

var p: Student = Student(age: 0)
print("[\(p.str())]")

[\(p.str())]")

