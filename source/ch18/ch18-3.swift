 class Person {
    var name: String = ""
    var age: Int = 0 {
        didSet {
            print("Person age:   \(self.age)")
        }
    }
}

class Student: Person{
    var grade: String = "F"

    override var age: Int {
        didSet{
            print("Student age: \(self.age)")
        }
    }
}

class School {
    var students: [Student] = [Student]()
    subscript(number: Int) -> Student {
        print("School subscript")
        return students[number]
    }
}

class MiddleSchool: School {
    var middleStudents: [Student] = [Student]()

    override subscript(index: Int) -> Student {
        print("MiddleSchool subscript")
        return middleStudents[index]
    }
}


let university: School = School()
university.students.append(Student())
university[0]

let middle: MiddleSchool = MiddleSchool()
middle.middleStudents.append(Student())
middle[0]


t())
middle[0]


