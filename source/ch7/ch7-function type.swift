 typealias CalculateTwoInts = (Int, Int) -> Int

func addTwoInts(_ a: Int, _ b: Int) -> Int {
    return a + b
}

func multiplyTwoInts(_ a: Int,_ b: Int) -> Int {
    return a * b
}


var mathFunction: CalculateTwoInts = addTwoInts
print(mathFunction(2,5))


mathFunction = multiplyTwoInts
print(mathFunction(2,5))

func printMathResult(_ mathFunctionX: CalculateTwoInts, _ a: Int, _ b: Int){
    print("Result : \(mathFunctionX(a, b))")
}
printMathResult(addTwoInts, 3,5)


func chooseMathFunction(_ toAdd: Bool) -> CalculateTwoInts {
    return toAdd ? addTwoInts : multiplyTwoInts
}
printMathResult(chooseMathFunction(false), 4, 5)

printMathResult(chooseMathFunction(true), 3, 5)



typealias MoveFunc = (Int) -> Int

func goRight(_ currentPosition: Int) -> Int {
    return currentPosition + 1
}

func goLeft(_ currentPosition: Int) -> Int {
    return currentPosition - 1
}

func functionForMove(_ shoudGoLeft: Bool) -> MoveFunc {
    return shoudGoLeft ? goLeft : goRight
}

var position : Int = 3

let moveToZero: MoveFunc = functionForMove(position > 0)

print("원점으로 갑시다")

while position != 0 {
    print("\(position)... ")
    position = moveToZero(position)
}

print("원점 도착")


func functionForMoveI(_ shouldGoLeft: Bool) -> MoveFunc {
    func goRight(_ currentPosition: Int) -> Int{
        return currentPosition + 1
    }

    func goLeft(_ currentPosition: Int) -> Int{
        return currentPosition - 1
    }

    return shouldGoLeft ? goLeft : goRight
}


var positionI: Int = -4

let moveToZeroI: MoveFunc = functionForMoveI(position > 0)

while positionI != 0{
    print("\(positionI)...")
    position = moveToZero(position)
}

////////////////////////////////////////////////////////////////////////

func crashAndBurn() -> Never {
    fatalError("Something very very bad happened")
}

crashAndBurn()

func someFunction(isAllIsWell: Bool) {
    guard isAllIsWell else {
        print("마을에 도둑이 들었습니다.")
        crashAndBurn()
    }
    print("All is well")
}

someFunction(isAllIsWell: true)
someFunction(isAllIsWell: false)


////////////////////////////////////////////////////////////////////////

func say(_ something: String) -> String{
    print(something)
    return something
}

@discardableResult func discadableResultSay(_ something: String) -> String {
    print(something)
    return something
}

say("Hello")

discadableResultSay("Hello1")


var result = discadableResultSay("Hello1")

print(result)

result = say("Hello2")

print(result)








