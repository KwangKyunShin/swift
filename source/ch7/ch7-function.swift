

func hello(name: String) -> String {
    return "Hello \(name)"
}

let helloJeny: String = hello(name: "Jenny")
print(helloJeny)


func introduce(name: String) -> String {
    return "제 이름은" + name + "입니다."
}

let introduceJenny: String = introduce(name: "Jenny")
print(introduceJenny)


func helloWorld() -> String {
    return "Hello World"
}

print(helloWorld())

////////////////////////////////////////////////////////////////

func sayHello(myName: String, yourName:String) -> String {
    return "Hello \(yourName)! I'm \(myName)"
}


func sayHello1(from myName:String , to yourName:String) -> String {
    return "Hello \(yourName) , I'm \(myName)"
}

print(sayHello1(from: "Shin", to: "Kim"))

////////////////////////////////////////////////////////////////
func sayHello2(_ name: String, _ times: Int) -> String {
    var result: String = ""
    for _ in 0 ..< times {
        result += "Hello \(name)!" + " "
    }
    return result
}
print(sayHello2("Chope", 2))
////////////////////////////////////////////////////////////////


func sayHello3(to name: String, _ times: Int) -> String{
    var result: String = ""
    for _ in 0 ..< times {
        result += "Hello \(name)!" + "\n"
    }
    return result
}

func sayHello4(to name: String, repeatCount times: Int) -> String {
    var result: String = ""

    for _ in 0 ..< times {
        result += "Hello \(name)!" + "\n"
    }

    return result
}

print(sayHello3(to: "Chope", 2))
print(sayHello4(to: "Chooog", repeatCount: 3))



//////////////////////////////////////////////////////////////////////////////

func sayHello5(_ name:String, times: Int = 3) -> String {
    var result: String = ""
    for _ in 0 ..< times{
        result += "Hello \(name)! \n"
    }
    return result
}


print(sayHello5("Hana"))

print(sayHello5("Jeo", times: 2))

//////////////////////////////////////////////////////////////////////////////

func sayHelloToFriends(me: String, friends names: String...) -> String{
    var result: String = ""
    for friend in names {
        result += "Hello \(friend) \n"
    }

    result += "I'm " + me + "!"
    return result
}


print(sayHelloToFriends(me: "Shin", friends: "John","Jay","Wizplan"))
print(sayHelloToFriends(me: "Yahoo"))

//////////////////////////////////////////////////////////////////////////////

var numbers:[Int] = [1,2,3]
func nonReferenceParameter(_ arr: [Int]) {
    var copiedArr:[Int] = arr
    copiedArr[1] = 1
    print(copiedArr)
}

func referenceParameter(_ arr: inout [Int]){
    arr[1] = 1
}


nonReferenceParameter(numbers)
print(numbers[1])
print(numbers)

referenceParameter(&numbers)
print(numbers[1])
print(numbers)



//////////////////////////////////////////////////////////////////////////////

func sayHelloWorld() {
    print("Hello World")
}

sayHelloWorld()

func sayHelloWorld(from myName: String , to name: String) {
    print(" \(myName) \(name)")
}
sayHelloWorld(from: "yagom", to: "MiJeong")

func sayGoodBye() -> Void {
    print("Good Bye")
}

sayGoodBye()







