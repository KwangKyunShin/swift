//ch27-9.swift
class Person {
	let name: String
	let hobby: String?

	lazy var introduce: () -> String = { [unowned self] in 
		var introduction: String = "My name is \(self.name)"

		guard let hobby = self.hobby else {
			return introduction
		}

		introduction += " "
		introduction += "My hobby is \(hobby)"
		return introduction
	}

	init(name: String, hobby: String? = nil) {
		self.name = name
		self.hobby = hobby
	}

	deinit {
		print("\(name) is being deinitialized")
	}
}

var yagom:  Person? = Person(name: "yagom", hobby: "eating")
print(yagom?.introduce())
yagom = nil

var hana: Person? = Person(name: "hana", hobby: "palying guiter")

hana?.introduce = yagom?.introduce ?? {" "}

print(yagom?.ontroduce())

yagom = nil
print(hana?.introduce())

