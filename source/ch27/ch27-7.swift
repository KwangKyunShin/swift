//ch27-7.swift

var a = 0
var b = 0
let closurex = { [a] in 
    print(a, b)
    b = 20
}

a = 10
b = 10
closurex()
print(b)

class SimpleClass {
    var value: Int = 0
}

var x = SimpleClass()
var y = SimpleClass()

let closure = { [x] in
    print(x.value, y.value)
}

x.value = 10
y.value = 10

closure()
