let string: String = "ABC"

switch string{
	case _: print(string)
}

let optionalString: String? = "ABC"

switch optionalString {
	case "ABC"?: print(optionalString)
	case _?: print("Has value, but not aBC")
	case nil: print("nil")
	//default: print("default")
}

let yagom = ("yagom", 99, "Male")

switch yagom {
	case ("yagom", _, _): print("Hello yagom!!")
	case (_, _, _): print("Who cares~")
	//default: print("default")
}

for _ in 0..<2 {
	print("Hello ")
}

