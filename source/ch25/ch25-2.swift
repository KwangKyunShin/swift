let yagom = ("yagom", 99, "Male")

switch yagom {
	case let (name, age, gender): print("Name: \(name), Age: \(age), Gender: \(gender)")
}

switch yagom {
	case (let name, let age, let gender): print("Name: \(name), Age: \(age), Gender: \(gender)")
}

switch yagom {
	case (let name, _, let gender): print("Name: \(name), Age:, Gender: \(gender)")
}