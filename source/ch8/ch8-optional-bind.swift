

let numbers:[Int?] = [2,nil,-4,nil,100]

for number in numbers {
    switch number {
    case .some(let value) where value < 0: print("Negative value! \(value)")
    case .some(let value) where value > 10: print("Large value!! \(value)")
    case .some(let value): print("Value \(value)")
    case .none: print("nil")
    }
}

////////////////////////////////////////////////////////////
var myName: String? = "yagom"

var yagom: String = myName!

myName = nil
yagom = myName!

if myName != nil {
    print("My name is \(myName!)")
}else {
    print("myName == nil")
}

/////////////////////////////////////////////////////////////

if let name = myName {
    print("My name is \(name)")
}else{
    print("myName == nil")
}

if var name = myName{
    name = "wizplan"
    print("My name is \(name)")
}else{
    print("myName == nil")
}


/////////////////////////////////////////////////////////////

var yourName: String? = nil

if let name = myName, let friend = yourName {
    print("We are friend! \(name) & \(friend)")
}

yourName = "eric"

if let name = myName, let friend = yourName {
    print("We are friend! \(name) & \(friend)")
}

///////////////////////////////////////////////////

print(myName)

myName = nil

if let name = myName{
    print("My name is \(name)")
}else {
    print("myName == nil")
}

myName?.isEmpty










































