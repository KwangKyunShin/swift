
var customersInLine: [String] = ["YoangWha", "SangYong", "SungHwn", "HaMi"]

print(customersInLine.count)
let customerProvider: () -> String = {
    return customersInLine.removeFirst()
}

let customerProvider1 = { () -> String in
    return customersInLine.removeFirst()
}
print(customersInLine.count)

print("Now serving \(customerProvider())!")
print(customersInLine.count)

print("Now serving \(customerProvider1())!")
print(customersInLine.count)




