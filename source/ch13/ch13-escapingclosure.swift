

func makeIncrementer(forIncrement amount: Int) -> (() -> Int){
    var runningTotal = 0
    func incrementer() -> Int {
        runningTotal += amount
        return runningTotal
    }
    return incrementer
}

let incrementByTwo: (() -> Int) = makeIncrementer(forIncrement: 2)
let incrementByTwo2: (() -> Int) = makeIncrementer(forIncrement: 2)
let incrementByTen: (() -> Int) = makeIncrementer(forIncrement: 10)
var first: Int = incrementByTwo()
var second: Int = incrementByTwo()
var third: Int = incrementByTwo()

first = incrementByTwo2()
second = incrementByTwo2()
third = incrementByTwo2()

first = incrementByTen()
second = incrementByTen()
third = incrementByTen()

var forth: Int = third
let t2:(() -> Int) = incrementByTen
var fifth: Int = t2()
print("forth: \(forth) \(fifth)")



/////////////////////////////////////////////////////////////
var completionHandlers:[() -> Void] = []

func someFunctionWithEscapingClosure(completionHandler: @escaping () -> Void) {
    completionHandlers.append(completionHandler)
}

typealias VoidVoidClosure = () -> Void

let firstClosure: VoidVoidClosure = {
    print("Closure A")
}

let secondClosure: VoidVoidClosure = {
    print("Closure B")
}

func returnOneClosure(first: @escaping VoidVoidClosure,
                      second: @escaping VoidVoidClosure,
                      shouldReturnFirstClosure: Bool) -> VoidVoidClosure {
    return shouldReturnFirstClosure ? first : second
}
let returnedClosure: VoidVoidClosure = returnOneClosure(first: firstClosure,
                                                        second: secondClosure,
                                                        shouldReturnFirstClosure: true)

returnedClosure()

var closures: [VoidVoidClosure] = []

func appendClosure(closure: @escaping VoidVoidClosure){
    closures.append(closure)
}

func functionWithNoescapeClosure(closure: VoidVoidClosure){
    closure()
}

func functionWithEscapingClosure(completionHandler: @escaping VoidVoidClosure) -> VoidVoidClosure{
    return completionHandler
}

class SomeClass {
    var x = 10
    
    func runNoescapeClosure() {
        functionWithNoescapeClosure { x = 200}
    }
    
    func runEscapingClosure() -> VoidVoidClosure {
        return functionWithEscapingClosure {
            self.x = 100
        }
    }
    
}


let instance: SomeClass = SomeClass()
instance.runNoescapeClosure()
print(instance.x)

let returnedClosure1: VoidVoidClosure = instance.runEscapingClosure()
returnedClosure1()
print(instance.x)




