

var customersInLine: [String] = ["YoangWha", "SangYong", " SungHun", "HaMi"]

func serveCustomer(_ customerProvider: () -> String){
    print("Now serving \(customerProvider())")
}
serveCustomer( {customersInLine.removeFirst() })
