
let names: [String] = ["wizplan","eric","yagom","jeny"]
func backwards(first: String, second: String) -> Bool{
    print("\(first) \(second) compare")
    return first > second
}

let reversed: [String] = names.sorted(by: backwards)
print(reversed)


let reversed1: [String] = names.sorted(by: {(first: String, second: String) -> Bool in return first < second})

print(reversed1)

let reversed2: [String] = names.sorted() {(first: String, second: String) -> Bool in
    return first > second
}

let reversed3: [String] = names.sorted{(first: String , second: String) -> Bool in
    return first > second
}

func doSomething(do: (String) -> Void,
                 onSuccess: (Any) -> Void,
                 onFailure: (Error) -> Void){
    //do SomeThing
}

doSomething{ (someStrng: String) in
    
}onSuccess: {(result: Any) in
    
}onFailure: {(error: Error) in
    
}
let reversed5: [String] = names.sorted{
    return $0 > $1
}

print(">>\(reversed5)")


let reversed6: [String] = names.sorted { $1 > $0}

print(">>\(reversed6)")

let reversed7: [String] = names.sorted(by:>)

print(">>\(reversed7)")
