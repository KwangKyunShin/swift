func doubledEven( _ num: Int) -> Int? {
    ifnum.isMultiple(of: 2) {
        return num * 2
    }
    return nil
}

Optional(3).flatMap(doubleEven)
