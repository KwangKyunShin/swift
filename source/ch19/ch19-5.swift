
protocol SomeProtocal {
    var settablePreperty: String{ get set }
    var notNeedToBeSettableProperty: String { get }
}

protocol AnotherProtocol {
    static var someTypeProperty: Int { get set }
    static var anotherTypeProperty: Int { get}
}

//protocol Sendable {
//    var from: String { get }
//    var to: String { get }
//}

protocol Sendable {
    var from: Sendable { get }
    var to: Receiveable? { get }

    func send (data: Any)

    static func isSendableInstance(_ instance: Any) -> Bool
}
/*
class Message: Sendable {
    var sender: String
    var from: String {
        return self.sender
    }
    var to: String

    init(sender: String, receiver: String) {
        self.sender = sender
        self.to = receiver
    }
}

class Mail: Sendable {
    var from: String
    var to: String

    init(sender: String, receiver: String) {
        self.from = sender
        self.to = receiver
    }
}
*/
protocol Receiveable {
    func received(data: Any, from: Sendable)
}

class Message: Sendable, Receiveable {
    var from: Sendable {
        return self
    }

    var to: Receiveable?

    func send(data: Any) {
        guard let receiver: Receiveable = self.to else {
            print("Message has no receiver ")
            return
        }

        receiver.received(data: data, from: self.from)
    }

    func received(data: Any, from: Sendable) {
        print("Message received \(data) from \(from)")
    }

    class func isSendableInstance(_ instance: Any) -> Bool {
        if let sendableInstance: Sendable = instance as? Sendable {
            return sendableInstance.to != nil
        }
        return false
    }

}


class Mail: Sendable, Receiveable {
    var from: Sendable {
        return self
    }

    var to: Receiveable?

    func send(data: Any) {
        guard let receiver: Receiveable = self.to else {
            print("Mail has no receiver")
            return
        }
        receiver.received(data: data, from: self.from)
    }

    static func isSendableInstance(_ instance: Any) -> Bool {
        if let sendableInstance: Sendable = instance as? Sendable {
            return sendableInstance.to != nil
        }
        return false
    }

}

let myPhoneMessage: Message = Message()
let yourPhoneMessage: Message = Message()

myPhoneMessage.send(data: "Hello")

myPhoneMessage.to = yourPhoneMessage
myPhoneMessage.send(data: "Hello")

let myMail: Mail = Mail()
let yourMail: Mail = Mail()

myMail.send(data: "Hi")

myMail.to = yourMail
myMail.send(data: "Hi")

myMail.to = myPhoneMessage
myMail.send(data: "Bye")

Message.isSendableInstance("Hello")


Message.isSendableInstance(myPhoneMessage)

Message.isSendableInstance(yourPhoneMessage)
Mail.isSendableInstance(myPhoneMessage)
Mail.isSendableInstance(myMail)




nce(myMail)




