protocol SomeProtocol{}

class SomeClass: SomeProtocol {}

let intType: Int.Type = Int.self
let stringType: String.Type = String.self
let classType: SomeClass.Type = SomeClass.self
let protocolProtocal: SomeProtocol.Protocol = SomeProtocol.self

var someType: Any.Type

someType = intType
print(someType)

someType = stringType
print(someType)

someType = classType
print(someType)

someType = protocolProtocal
print(someType)

print(type(of: coffee) == Coffee.self)
print(type(of: coffee) == Americano.self)
print(type(of: coffee) == Latte.self)

print(type(of: coffee) == Americano.self)
print(type(of: myCoffee) == Americano.self)
print(type(of: yourCoffee) == Americano.self)

print(type(of: coffee) == Latte.self)
print(type(of: myCoffee) == Latte.self)
print(type(of: yourCoffee) == Latte.self)


if let actionOne: Americano = Coffe as? Americano {
    print("This is Americano")
}else{
    print(coffe.description)
}

if let actingOne: Latte = coffe as? Latte {
    print("This is Latte")
}else {
    print(coffee.description)
}

if let actingOne: Coffe = as? Coffe {
    print("This is Just Coffee")
}else{
    print(coffee.description)
}

if let actingOne: Americano = myCoffee as? Americano {
    print("This is Americano")
}else{
    print(coffee.description)
}

if let actingOne: Latte = myCoffee as? Latte {
    print("This is Latte")
}else {
    print(coffee.description)
}

if let actingOne: Coffee = myCoffee as? Coffee {
    print("This is Just Coffee")
}else{
    print(coffee.description)
}

let castedCoffee: Coffee = yourCoffee as! Coffee

let castedAmericano: Americano = coffee as! Americano









ricano









