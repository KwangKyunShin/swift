protocol Talkable {}
protocol CallToAll {
    func callToAll()
}


struct Person: Talkable {}

struct Animal {}

extension Array: CallToAll where Element: Talkable {
	func callToAll() {}
}

let people: [Person] = []
//let cats: [Animal] = []

people.callToAll()
//cat.callToAll()
