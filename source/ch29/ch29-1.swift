//ch29-1.swift

var one: Int = 1

print("숫자 출력: \(one)")

func oneMore( than number: Int) -> Int {
	return number + 1
}

var myNumber: Int = 1
myNumber = oneMore(than: myNumber)
print(myNumber)


var number: Int = 100
print(Unmanaged<AnyObject>.fromOpaque(&number).toOpaque())


var object: SomeClass = SomeClass()
print(Unmanaged<AnyObject>.passUnretained(object).toOpaque())